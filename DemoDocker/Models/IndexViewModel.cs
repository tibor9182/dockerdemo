﻿namespace DemoDocker.Models
{
    public class IndexViewModel
    {
        public string Env { get; set; }

        public bool ShowEnv => !string.IsNullOrEmpty(Env);
    }
}
